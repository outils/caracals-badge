module.exports = {
  entry: './main-badge.js',
  output: {
    filename: './caracals-badge.js'
  },
  module: {
    rules:[
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ]
      },
    ]
  }
};
