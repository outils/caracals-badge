
Inspired by: [github-fork-ribbon-css](https://github.com/simonwhitaker/github-fork-ribbon-css)
Using [Webpack](https://webpack.js.org/)


Install
=======


    npm install .


Build
=====

    npm run prod

The JS bundle is then located in `dist/` folder.


Usage
=====
Just add (orientation can be `left-bottom`, `right-bottom`, `right-top` or `left-top`):

```html
<script src="path/to/caracals-badge.js" orientation="left-bottom"></script>
```

